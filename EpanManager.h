#ifndef EPANMANAGER_H
#define	EPANMANAGER_H

#define MAX_PACKET_SIZE 100000
#include "pcap2pdml.h"

class EpanManager
{
public: 
    /**
     * Constructor
     * @param wireshark_profile
     * @param dfilter
     * @param p_max_8mb_blocks
     * @param p_ID
     */
    EpanManager(const std::string & wireshark_profile, 
                const std::string & dfilter, 
                const size_t p_max_8mb_blocks, 
                const std::string & p_ID);
    
    /**
     * 
     * @param pcap2pdml_ctxt
     * @param epan_max_8MB_blocks
     * @param ws_dfilter
     */
    void check_epan_reset(void * pcap2pdml_ctxt, 
                          const size_t epan_max_8MB_blocks, 
                          const char * ws_dfilter);
    
    /**
     * 
     */
    void check_epan_reset();
    
    /**
     * 
     * @return 
     */
    pcap2pdml * GetParser()
    {
        return parser;
    }
    
    /**
     * Init EPAN
     */
    void init();
    
    /**
     * Terminate EPAN
     */
    void term();
    
    /**
     * Wait for raw Data from fides-sniffer
     * @param fdr The read file descriptor
     * @return number of bytes read or 0 if not
     */
    ssize_t WaitForPacket(int fdr);
    
    /**
     * Call EPAN to convert buffer to JSON
     */
    void ProcessPacket();
    
    /**
     * Send the JSON to fides-sniffer
     * @param fdw The write file descriptor
     */
    void SendResult(int fdw);
    
    /**
     * Set the Sniffer mode Mirror or Inline
     * @param mirror
     */
    void SetSnifferMode(bool mirror);
    
private:
    
    /**
     * 
     * @param data
     * @param size
     */
    void PrintData(unsigned char * data, int size);
    
    std::string ws_profile;
    std::string ws_dfilter;
    size_t max_8mb_blocks;
    std::string epan_output;
    pcap2pdml *parser;
    void * pcap2pdml_ctxt;
    std::string ID;
    unsigned char buf[MAX_PACKET_SIZE];
    std::string Packet_Output;
    bool is_running;
    bool isMirror;
    uint32_t counter_print;
};



#endif	/* EPANMANAGER_H */

