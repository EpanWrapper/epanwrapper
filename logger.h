
#ifndef LOGGER_H
#define	LOGGER_H

#include "log4cpp/Category.hh"
#include "log4cpp/Appender.hh"
#include "log4cpp/FileAppender.hh"
#include "log4cpp/OstreamAppender.hh"
#include "log4cpp/Layout.hh"
#include "log4cpp/BasicLayout.hh"
#include "log4cpp/Priority.hh"

#define _ERROR (log4cpp::Priority::ERROR)
#define _DEBUG (log4cpp::Priority::DEBUG)
#define _INFO (log4cpp::Priority::INFO)
#define _WARN (log4cpp::Priority::WARN)


extern log4cpp::Category& root;
#define LOG(LEVEL) root << LEVEL

extern log4cpp::Priority::Value currentLogLevel;

#endif	/* LOGGER_H */

