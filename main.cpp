#include <cstdlib>
#include <string.h>
#include "logger.h"
#include "EpanManager.h"
#include "analyzer_config.h"
#include "statistics.h"

#include <chrono>
#include <cerrno>
#include <log4cpp/RollingFileAppender.hh>
#include <log4cpp/PatternLayout.hh>

#include <boost/algorithm/string.hpp>

#include "Configuration/ConfigurationLoader.h"
#include "Configuration/ConfigSingleton.h"

/*
 * arguments: 
 * 1. profile name (like mms) (-C)
 * 2. format name (like pdml or json) (-T)
 * 3. input pipe name (-in)
 * 4. output pipe name  (-out)
 * 5. Display filter (-d)
 * 6. Size (-size)
 * 7. ID (-id)
 */

log4cpp::Category& root = log4cpp::Category::getRoot();
log4cpp::Priority::Value currentLogLevel = log4cpp::Priority::INFO;

void init_logger()
{
    log4cpp::Appender *appender1 = new log4cpp::OstreamAppender("console", &std::cout);
    appender1->setLayout(new log4cpp::BasicLayout());
    const std::string logLevel = fides::ConfigSingleton::getConfigurationTree()->get<std::string>(analyzer::Log::LogLevel);
    if (!strcasecmp(logLevel.c_str(), "DEBUG"))
    {
        currentLogLevel = log4cpp::Priority::DEBUG;
    }
    root.setPriority(currentLogLevel);
    root.addAppender(appender1);
}

void add_appender(const std::string & ID)
{
    const std::string logfilename = analyzer::LogFileNameBase + ID + ".log";
    log4cpp::RollingFileAppender *appender2 = new log4cpp::RollingFileAppender("default", logfilename);
    appender2->setMaximumFileSize(atoi(fides::ConfigSingleton::getConfigurationTree()->get<std::string>(analyzer::Log::MaxFileSize).c_str()));
    appender2->setMaxBackupIndex(atoi(fides::ConfigSingleton::getConfigurationTree()->get<std::string>(analyzer::Log::FileCount).c_str()));
    // Instantiate a layout object
    log4cpp::PatternLayout *patternLayout = new log4cpp::PatternLayout;
    patternLayout->setConversionPattern("%d %-5p %m%n");
    // Attach the layout object to the appender object
    appender2->setLayout(patternLayout);
    root.addAppender(appender2);
    LOG(_INFO) << "Analyzer ID: " << ID << " started";
    log4cpp::Appender *appender1 = log4cpp::Appender::getAppender("console");
    root.removeAppender(appender1);
}

void write_pid_file(std::string& ID)
{
    std::ofstream pid_file;
    pid_file.open(analyzer::PidFileNamePrefix + ID + analyzer::PidFileNameSuffix);
    pid_file << getpid();
    pid_file.close();
}

int main(int argc, char** argv)
{
    std::string Profile;
    std::string DFilter;
    std::string IN_Pipe;
    std::string OUT_Pipe;
    std::string OutputFormat;
    size_t EpanMax8MBBlocks;
    std::string ID;

    // Reading App Parameters:

    // Minimum number of arguments - 4 - we want profile, output format and 2 pipes.
    if (argc < 4)
    { 
        // Check the value of argc. If not enough parameters have been passed, inform user and exit.
        std::cerr << "Not enough arguments to start fides-analyzer"; // Inform the user of how to use the program
        return -1;
    }

    fides::ConfigurationLoader loader(analyzer::FidesConfigFile);
    fides::ConfigSingleton::initialize(loader);

    // Initialize Logger:
    init_logger();
   
    std::cout << "Analyzer Started, argc: " << argc << std::endl;

    for (int i = 1; i < argc; ++i)
    {
        if (i + 1 != argc)
        {
            if (!strcmp(argv[i], "-C"))
            {
                Profile = argv[i + 1];
                ++i;
                continue;
            }
            if (!strcmp(argv[i], "-in"))
            {
                IN_Pipe = argv[i + 1];
                //LOG(_DEBUG) << "in command: " << argv[i + 1] << " IN_Pipe: " << IN_Pipe;
                ++i;
                continue;
            }
            if (!strcmp(argv[i], "-out"))
            {
                OUT_Pipe = argv[i + 1];
                ++i;
                continue;
            }
            if (!strcmp(argv[i], "-d"))
            {
                DFilter = argv[i + 1];
                ++i;
                continue;
            }
            if (!strcmp(argv[i], "-size"))
            {
                std::string strMax8M = argv[i + 1];
                EpanMax8MBBlocks = strtol(strMax8M.c_str(), NULL, 10);
                ++i;
                continue;
            }
            if (!strcmp(argv[i], "-id"))
            {
                ID = argv[i + 1];
                ++i;
                continue;
            }
        }
    }
    add_appender(ID);
    LOG(_DEBUG) << "argc: " << argc;
    LOG(_DEBUG) << "Profile: " << Profile << ", In: " << IN_Pipe 
                << ", Out: " << OUT_Pipe
                << ", DFilter: " << DFilter   
                << ", EpanMax8MBBlocks: " << EpanMax8MBBlocks;
    LOG(_DEBUG) << "Finished Parsing Arguments";
    
    StatsSingleton::instance().CreateStatsLog(ID);
    StatsSingleton::instance().Start();
    
    // Initialize EPAN:
    EpanManager *myManager = new EpanManager(Profile, DFilter, EpanMax8MBBlocks, ID);
    myManager->init();

    std::string type = fides::ConfigSingleton::getConfigurationTree()->get<std::string>(analyzer::SnifferType);
    if (boost::iequals(type, "NetFilter"))
    {
        myManager->SetSnifferMode(false);
    }
    else
    {
        myManager->SetSnifferMode(true);
    }

    // Open Pipes
    LOG(_DEBUG) << "Opening input pipe: " << IN_Pipe;
    int fdr = open(IN_Pipe.c_str(), O_NONBLOCK | O_RDWR);
    if (fdr < 0)
    {
        LOG(_ERROR) << "Analyzer Reader Open Failed: " << ID << ". Error: " << strerror(errno);
    }
    else
    {
        LOG(_DEBUG) << "Analyzer Reader Open: " << ID;
    }

    LOG(_DEBUG) << "Opening writer pipe: " << OUT_Pipe;
    int fdw = open(OUT_Pipe.c_str(), O_WRONLY);
    if (fdw < 0)
    {
        LOG(_ERROR) << "Analyzer Writer Open Failed: " << ID << ". Error: " << strerror(errno);
    }
    else
    {
        LOG(_DEBUG) << "Analyzer Writer Open: " << ID;
    }
    
    long CheckEpanResetCounter = 0;
    write_pid_file(ID);
    while (1)
    {
        // get packet
        std::size_t pSize = myManager->WaitForPacket(fdr);
        if (0 == pSize)
        {
            continue;
        }

        // global buf holds the packet
        myManager->ProcessPacket();
       
        myManager->SendResult(fdw);
        
        if (++CheckEpanResetCounter % 10000 == 0)
        {
            myManager->check_epan_reset();
            CheckEpanResetCounter = 1;
        }
    }
    myManager->term();
    StatsSingleton::instance().Stop();

    return 0;
}
