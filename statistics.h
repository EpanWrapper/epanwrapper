#ifndef STATISTICS_H
#define	STATISTICS_H

#include "../../../lib/shared/singleton.h"

#include <log4cpp/Category.hh>

#include <boost/atomic/atomic.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/circular_buffer.hpp>

#include <array>

enum StatsKey 
{ 
    KEYZERO = 0,
    RecPacketOk,
    RecPacketError,
    RecPacketBadSync,
    EpanProcessPacket,
    EpanError,
    EpanEmpty,
    EpanOk,
    EpanRetransmit,
    SentOk,
    SentError,
    LAST_STATSKEY_VALUE
};

class Statistics
{
private:
    friend infra::patterns::Singleton<Statistics>;

    Statistics();

public:
    typedef uint64_t ValueType;
    
	/**
     * Change a counter value 
     * @param key The counter key
     * @param value The new value
     * @return the value before assignment
     */
    ValueType SetValue(StatsKey key, ValueType value);

	/**
     * Get the current counter value
     * @param key The counter key
     * @return The current value
     */
    ValueType GetValue(StatsKey key);

    /**
     * Increment the current counter value by some delta
     * @param key The counter key
     * @param delta The delta
     */
    void Increment(StatsKey key, uint16_t delta = 1);

    /**
     * Decrement the current counter value by some delta
     * @param key The counter key
     * @param delta The delta
     */
    void Decrement(StatsKey key, uint16_t delta = 1);

    /** 
     * Start the thread
     */
    void Start();

    /** 
     * Stop the thread
     */
    void Stop();

    /**
     * Thread function
     */ 
    void operator () ();
    
    /**
     * Init the stats logs
     * @param ID
     */
    void CreateStatsLog(const std::string & ID);
    
private:
    typedef boost::atomic<ValueType> AtomicInt;
    typedef std::shared_ptr<AtomicInt> AtomicIntPtr;
    typedef std::array<AtomicIntPtr, LAST_STATSKEY_VALUE> AtomicStatsMap; 
    
     // Statistics log interval
    uint16_t logInterval_;
    
    // The Statistics container
    AtomicStatsMap statsMap_;

    // The time of the last log of statistics
    time_t lastLogTime_; 

    boost::shared_ptr<boost::thread> statisticsThread_;  
    boost::atomic<bool> isRunning_;
    
    /**
     * Log to statistics file all the counter values formatted
     */
    void LogStats();
    std::string rec_log();
    std::string epan_log();
    std::string sent_log();
    
    /**
     * Initiate the statistics log
     */
    void CreateLog();
};

typedef infra::patterns::Singleton<Statistics> StatsSingleton;

#endif	/* STATISTICS_H */

