#ifndef ANALYZER_CONFIG_H
#define	ANALYZER_CONFIG_H

namespace analyzer
{
    static std::string const BinaryPath                    = "/usr/bin/";
    static std::string const ConfigPath                    = "/etc/fides/";
    static std::string const FidesConfigFileName           = "fides-sniffer.config";
    static std::string const FidesConfigFile               = ConfigPath + FidesConfigFileName;
    static std::string const LogFolderPath                 = "/var/log/fides/";
    static std::string const LogFileNameBase               = LogFolderPath + "fides-analyzer_";
    static std::string const StatsFileNameBase             = LogFolderPath + "fides-analyzer-stats_";
    static std::string const PidFileNamePrefix             = "/var/run/fides/analyzer";
    static std::string const PidFileNameSuffix             = ".pid";

    namespace Log
    {
        static std::string const StatsFilename             = "Configuration.Logger.StatsFilename";
        static std::string const StatsIntervalTime         = "Configuration.Logger.StatsIntervalTime";
        static std::string const FileCount                 = "Configuration.Logger.FileCount";
        static std::string const MaxFileSize               = "Configuration.Logger.MaxFileSize";
        static std::string const DdsFilename               = "Configuration.Logger.DdsFilename";
        static std::string const LogLevel                  = "Configuration.Logger.LogLevel";
    }
    
    static std::string const SnifferType                   = "Configuration.Sniffers.Type"; //This can be Netfilter or Pcap
}

#endif	/* ANALYZER_CONFIG_H */

