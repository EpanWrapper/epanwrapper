
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <pthread.h>
#include <pcap.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/uio.h>
#include <string>
#include <pcap/pcap.h>
#include "logger.h"
#define WS_MSVC_NORETURN

#include <wireshark/register.h>

#include <wireshark/epan/epan.h>
#include <wireshark/epan/prefs.h>
#include <wireshark/epan/timestamp.h>
#include <wireshark/epan/frame_data.h>
#include <wireshark/config.h>
#include <wireshark/epan/packet.h>
#include <wireshark/epan/print.h>
#include <wireshark/epan/tvbuff.h>
#include <wireshark/epan/decode_as.h>
#include <wireshark/epan/prefs-int.h>
#include <wireshark/epan/dfilter/dfilter.h>

#include <wireshark/wiretap/wtap.h>

#include <wireshark/wsutil/nstime.h>
#include <wireshark/wsutil/privileges.h>
#include <wireshark/wsutil/plugins.h>
#include <wireshark/wsutil/report_message.h>
#include <wireshark/wsutil/filesystem.h>

#include "pcap2pdml.h"


//=========================================================================================//

/*
 * The code in the following section, the decode_as file loading,
 * was copied and edited from wireshark GUI application.
 * The following items are included:
 * 1. the lookup_entry___t struct type definition.
 * 2. the change_dissector_if_matched__() function
 * 3. the read_set_decode_as_entries__() function
 * 4. the read_prefs_file__() function
 * 5. the load_decode_as_file() function
 *
 * The original code in wireshark is from the decode_as_utils.c/h
 * Wireshark did not share that code in a library hence it had to be copied.
 * It was also edited to have unnecessary bits and pieces removed.
 */


typedef struct lookup_entry__
{
    gchar*             dissector_short_name;
    dissector_handle_t handle;
} lookup_entry___t;

void change_dissector_if_matched__(gpointer item, gpointer user_data)
{
    auto handle = (dissector_handle_t)item;
    auto * lookup = (lookup_entry___t *)user_data;
    if (strcmp(lookup->dissector_short_name, dissector_handle_get_short_name(handle)) == 0) {
        lookup->handle = handle;
    }
}

prefs_set_pref_e read_set_decode_as_entries__(gchar *key, const gchar *value, void *user_data, gboolean return_range_errors)
{
    gchar *values[4] = {NULL, NULL, NULL, NULL};
    gchar delimiter[4] = {',', ',', ',','\0'};
    gchar *pch;
    guint i, j;
    dissector_table_t sub_dissectors;
    prefs_set_pref_e retval = PREFS_SET_OK;
    gboolean is_valid = FALSE;

    if (strcmp(key, DECODE_AS_ENTRY) == 0)
    {
        /* Parse csv into table, selector, initial, current */
        for (i = 0; i < 4; i++)
        {
            pch = (gchar *)strchr(value, delimiter[i]);
            if (pch == NULL)
            {
                for (j = 0; j < i; j++)
                {
                    g_free(values[j]);
                }
                return PREFS_SET_SYNTAX_ERR;
            }
            values[i] = g_strndup(value, pch - value);
            value = pch + 1;
        }
        sub_dissectors = find_dissector_table(values[0]);
        if (sub_dissectors != NULL)
        {
            lookup_entry___t lookup;
            ftenum_t selector_type;

            lookup.dissector_short_name = values[3];
            lookup.handle = NULL;
            selector_type = dissector_table_get_type(sub_dissectors);

            g_slist_foreach(dissector_table_get_dissector_handles(sub_dissectors), change_dissector_if_matched__, &lookup);
            if (lookup.handle != NULL || g_ascii_strcasecmp(values[3], DECODE_AS_NONE) == 0)
            {
                is_valid = TRUE;
            }

            if (is_valid)
            {
                if (IS_FT_STRING(selector_type))
                {
                    dissector_change_string(values[0], values[1], lookup.handle);
                }
                else
                {
                    char *p;
                    long long_value;

                    long_value = strtol(values[1], &p, 0);
                    if (p == values[0] || *p != '\0' || long_value < 0 || (unsigned long)long_value > UINT_MAX)
                    {
                        retval = PREFS_SET_SYNTAX_ERR;
                        is_valid = FALSE;
                    }
                    else
                    {
                        dissector_change_uint(values[0], (guint)long_value, lookup.handle);

                        dissector_handle_t hcheck = dissector_get_uint_handle(sub_dissectors, (guint)long_value);
                        if (hcheck != lookup.handle)
                        {
                            retval = prefs_set_pref_e(7);
                        }
                    }
                }
            }
        }
        else
        {
            retval = PREFS_SET_SYNTAX_ERR;
        }
    }
    else
    {
        retval = PREFS_SET_NO_SUCH_PREF;
    }

    for (i = 0; i < 4; i++)
    {
        g_free(values[i]);
    }
    return retval;
}

int read_prefs_file__(const char *pf_path, FILE *pf, pref_set_pair_cb pref_set_pair_fct, void *private_data)
{
    enum {
        START,    /* beginning of a line */
        IN_VAR,   /* processing key name */
        PRE_VAL,  /* finished processing key name, skipping white space before value */
        IN_VAL,   /* processing value */
        IN_SKIP   /* skipping to the end of the line */
    } state = START;
    int       got_c;
    GString  *cur_val;
    GString  *cur_var;
    gboolean  got_val = FALSE;
    gint      fline = 1, pline = 1;
    gchar     hint[] = "(save preferences to remove this warning)";
    gchar     ver[128];

    cur_val = g_string_new("");
    cur_var = g_string_new("");

    /* Try to read in the profile name in the first line of the preferences file. */
    if (fscanf(pf, "# Configuration file for %127[^\r\n]", ver) == 1) {
        /* Assume trailing period and remove it */
        g_free(prefs.saved_at_version);
        prefs.saved_at_version = g_strndup(ver, strlen(ver) - 1);
    }
    rewind(pf);

    while ((got_c = getc(pf)) != EOF) {
        if (got_c == '\r') {
            /* Treat CR-LF at the end of a line like LF, so that if we're reading
             * a Windows-format file on UN*X, we handle it the same way we'd handle
             * a UN*X-format file. */
            got_c = getc(pf);
            if (got_c == EOF)
                break;
            if (got_c != '\n') {
                /* Put back the character after the CR, and process the CR normally. */
                ungetc(got_c, pf);
                got_c = '\r';
            }
        }
        if (got_c == '\n') {
            state = START;
            fline++;
            continue;
        }

        switch (state) {
        case START:
            if (g_ascii_isalnum(got_c)) {
                if (cur_var->len > 0) {
                    if (got_val) {
                        if (cur_val->len > 0) {
                            if (cur_val->str[cur_val->len-1] == ',') {
                                /*
                                 * If the pref has a trailing comma, eliminate it.
                                 */
                                cur_val->str[cur_val->len-1] = '\0';
                                LOG(_WARN) << pf_path << " line " << pline << ": trailing comma in \"" << cur_var->str << "\" " << hint;
                            }
                        }
                        /* Call the routine to set the preference; it will parse
                           the value as appropriate.

                           Since we're reading a file, rather than processing
                           explicit user input, for range preferences, silently
                           lower values in excess of the range's maximum, rather
                           than reporting errors and failing. */
                        switch (pref_set_pair_fct(cur_var->str, cur_val->str, private_data, FALSE)) {

                        case PREFS_SET_OK:
                            break;

                        case PREFS_SET_SYNTAX_ERR:
                            {
                                LOG(_WARN) << "Syntax error in preference \"" << cur_var->str << "\" at line " << pline << " of " << pf_path << " " << hint;
                            }
                            break;

                        case PREFS_SET_NO_SUCH_PREF:
                            /*
                             * If "print.command" silently ignore it because it's valid
                             * on non-Win32 platforms.
                             */
                            if (strcmp(cur_var->str, "print.command") != 0)
                            {
                                LOG(_WARN) << "No such preference \"" << cur_var->str << "\" at line " << pline << " of " << pf_path << " " << hint;
                            }
                            prefs.unknown_prefs = TRUE;
                            break;

                        case PREFS_SET_OBSOLETE:
                            if (strcmp(cur_var->str, "print.command") != 0)
                            {
                                /* If an attempt is made to save the preferences, a popup warning will be
                                   displayed stating that obsolete prefs have been detected and the user will
                                   be given the opportunity to save these prefs under a different profile name.
                                   The prefs in question need to be listed in the console window so that the
                                   user can make an informed choice.
                                */
                                LOG(_WARN) << "Obsolete preference \"" << cur_var->str << "\" at line " << pline << " of " << pf_path << " " << hint;
                            }
                            prefs.unknown_prefs = TRUE;
                            break;
                        }
                    } else {
                        LOG(_WARN) << "Incomplete preference at line " << pline << " of " << pf_path << " " << hint;
                    }
                }
                state      = IN_VAR;
                got_val    = FALSE;
                g_string_truncate(cur_var, 0);
                g_string_append_c(cur_var, (gchar) got_c);
                pline = fline;
            } else if (g_ascii_isspace(got_c) && cur_var->len > 0 && got_val) {
                state = PRE_VAL;
            } else if (got_c == '#') {
                state = IN_SKIP;
            } else {
                LOG(_WARN) << "Malformed preference at line " << fline << " of " << pf_path << " " << hint;
            }
            break;
        case IN_VAR:
            if (got_c != ':') {
                g_string_append_c(cur_var, (gchar) got_c);
            } else {
                /* This is a colon (':') */
                state   = PRE_VAL;
                g_string_truncate(cur_val, 0);
                /*
                 * Set got_val to TRUE to accommodate prefs such as
                 * "gui.fileopen.dir" that do not require a value.
                 */
                got_val = TRUE;
            }
            break;
        case PRE_VAL:
            if (!g_ascii_isspace(got_c)) {
                state = IN_VAL;
                g_string_append_c(cur_val, (gchar) got_c);
            }
            break;
        case IN_VAL:
            g_string_append_c(cur_val, (gchar) got_c);
            break;
        case IN_SKIP:
            break;
        }
    }
    if (cur_var->len > 0) {
        if (got_val) {
            /* Call the routine to set the preference; it will parse
               the value as appropriate.

               Since we're reading a file, rather than processing
               explicit user input, for range preferences, silently
               lower values in excess of the range's maximum, rather
               than reporting errors and failing. */
            switch (pref_set_pair_fct(cur_var->str, cur_val->str, private_data, FALSE)) {

            case PREFS_SET_OK:
                break;

            case PREFS_SET_SYNTAX_ERR:
                LOG(_WARN) << "Syntax error in preference \"" << cur_var->str << "\" at line " << pline << " of " << pf_path << " " << hint;
                break;

            case PREFS_SET_NO_SUCH_PREF:
                LOG(_WARN) << "No such preference \"" << cur_var->str << "\" at line " << pline << " of " << pf_path << " " << hint;
                prefs.unknown_prefs = TRUE;
                break;

            case PREFS_SET_OBSOLETE:
                LOG(_WARN) << "Obsolete preference \"" << cur_var->str << "\" at line " << pline << " of " << pf_path << " " << hint;
                prefs.unknown_prefs = TRUE;
                break;

            default:
                LOG(_WARN) << "Undefined error with preference \"" << cur_var->str << "\" at line " << pline << " of " << pf_path << " " << hint;
                prefs.unknown_prefs = TRUE;
                break;
            }
        }
        else
        {
            LOG(_WARN) << "Incomplete preference at line " << pline << " of " << pf_path << " " << hint;
        }
    }

    g_string_free(cur_val, TRUE);
    g_string_free(cur_var, TRUE);

    if (ferror(pf))
        return errno;
    else
        return 0;
}


void load_decode_as_file(const char * profile_name)
{
    std::string profile_folder = get_profiles_dir() + std::string("//") + std::string(profile_name);

    set_persconffile_dir(profile_folder.c_str());
    char * da_file_name = get_persconffile_path(DECODE_AS_ENTRIES_FILE_NAME, FALSE);
    if (nullptr != da_file_name)
    {
        FILE * da_file = fopen(da_file_name, "r");
        if (nullptr != da_file)
        {
            if (0 != read_prefs_file__(da_file_name, da_file, read_set_decode_as_entries__, nullptr))
            {
                LOG(_ERROR) << "Fail to read preferences";
            }
            fclose(da_file);
        }
        else
        {
            LOG(_ERROR) << "Failed to open the 'decode-as' file [" << da_file_name << "].";
        }
        g_free(da_file_name);
    }
    else
    {
        LOG(_ERROR) << "Failed to locate the 'decode-as' file [" << DECODE_AS_ENTRIES_FILE_NAME << "].";
    }
}

//=========================================================================================//

typedef struct
{
    epan_t * epan_session;
    u_int32_t packet_count;
    u_int32_t byte_count;
    nstime_t elapsed;
    dfilter_t * dfcode;
}parser_context_t;

//-----------------------------------------------------------------------------------------//

void report_failure(const char * msg, va_list)
{
    LOG(_ERROR) << "Wireshark: " << msg;
}

void report_warning(const char * msg, va_list)
{
    LOG(_WARN) << "Wireshark: " << msg;
}

void report_open_failure(const char * msg, int x, gboolean b)
{
    LOG(_ERROR) << "Wireshark: " << msg;
}

void report_read_failure(const char * msg, int x)
{
    LOG(_ERROR) << "Wireshark: " << msg;
}

void report_write_failure(const char * msg, int x)
{
    LOG(_ERROR) << "Wireshark: " << msg;
}

static bool initialize_epan(const char * profilename)
{
    setenv("G_SLICE", "always-malloc", 1);
    setenv("G_DEBUG", "gc-friendly", 1);
    setenv("HOME", "/etc/fides", 1);
    // TODO try to remove also the ".config" prefix: /etc/fides/.config/wireshark/profiles

    //set timestamp type
    timestamp_set_type(TS_RELATIVE);
    timestamp_set_precision(TS_PREC_AUTO);
    timestamp_set_seconds_type(TS_SECONDS_DEFAULT);

    init_process_policies();
    init_report_message(report_failure, report_warning, report_open_failure, report_read_failure, report_write_failure);

    const char * fp = get_profiles_dir();
    LOG(_DEBUG) << "Epan profiles folder path: " << fp;

    if (profilename && 0 < strlen(profilename) && profile_exists(profilename, FALSE))
    {
        set_profile_name(profilename);
        LOG(_DEBUG) << "Set profile name: " << profilename;
    }
    else
    {
        LOG(_WARN) << "Cannot found MMS profile in path: " << fp;
    }

    // TRUE loads wiretap plugins, might need to be changed to FALSE
    wtap_init(TRUE);
    LOG(_DEBUG) << "wtap_init success";

    if (!epan_init(register_all_protocols, register_all_protocol_handoffs, nullptr, nullptr))
    {
        LOG(_ERROR) << "Cannot init EPAN";
        return false;
    }
    LOG(_DEBUG) << "epan_init success";

    e_prefs *ep = epan_load_settings();

    if (ep == nullptr)
    {
        LOG(_ERROR) << "Can't open global preferences file";
        return false;
    }

    LOG(_DEBUG) << "read_prefs success";

    load_decode_as_file(profilename);

    /* Notify all registered modules that have had any of their preferences
    changed either from one of the preferences file or from the command
    line that their preferences have changed. */
    prefs_apply_all();

    return true;
}

//-----------------------------------------------------------------------------------------//

int pcap2pdml::init_parser(void * * parser_ctxt, const char * profilename, const char * dfilter)
{

    if (!initialize_epan(profilename))
    {
        return -1;
    }

    static const struct packet_provider_funcs funcs = {
            nullptr,
            nullptr,
            nullptr,
            nullptr,
    };

    auto * pctxt = (parser_context_t *)malloc(sizeof(parser_context_t));
    if (nullptr != pctxt)
    {
        if (nullptr != (pctxt->epan_session = epan_new(nullptr, &funcs)))
        {
            pctxt->packet_count = pctxt->byte_count = 0;
            nstime_set_zero(&pctxt->elapsed);
            *parser_ctxt = pctxt;
            if (dfilter_compile(dfilter, &pctxt->dfcode, nullptr) == -1)
            {
                LOG(_ERROR) << "dfilter_compile() failed: [" << dfilter << "]";
            }
            return 0;
        }
        free(pctxt);
    }
    return -1;
}

//-----------------------------------------------------------------------------------------//

int pcap2pdml::re_init_parser(void * parser_ctxt, const char * dfilter)
{
    auto * pctxt = (parser_context_t *)parser_ctxt;
    if (nullptr != pctxt)
    {
        if (nullptr != pctxt->dfcode)
        {
            dfilter_free(pctxt->dfcode);
            pctxt->dfcode = nullptr;
        }
        if (nullptr != pctxt->epan_session)
        {
            epan_free(pctxt->epan_session);
            pctxt->epan_session = nullptr;
        }
        pctxt->packet_count = pctxt->byte_count = 0;
        nstime_set_zero(&pctxt->elapsed);

        static const struct packet_provider_funcs funcs = {
                nullptr,
                nullptr,
                nullptr,
                nullptr,
        };

        if (nullptr == (pctxt->epan_session = epan_new(nullptr, &funcs)))
        {
            return -1;
        }
        if (dfilter_compile(dfilter, &pctxt->dfcode, nullptr) == -1)
        {
            LOG(_ERROR) << "dfilter_compile() failed: [" << dfilter << "]";
        }
    }
    return 0;
}

//-----------------------------------------------------------------------------------------//

int pcap2pdml::parse_packet(void * parser_ctxt,
                            const struct pcap_pkthdr * pkt_hdr,
                            const u_int8_t * pkt_data,
                            std::string & outBuf)
{
    int result = -1;
    outBuf.clear();
    gboolean passed = TRUE;
    auto * pctxt = (parser_context_t *)parser_ctxt;
    pctxt->packet_count++;

    struct wtap_pkthdr wtap_hdr;
    wtap_phdr_init(&wtap_hdr);
    wtap_hdr.rec_type = REC_TYPE_PACKET;
    wtap_hdr.ts.secs = pkt_hdr->ts.tv_sec;
    wtap_hdr.ts.nsecs = pkt_hdr->ts.tv_usec*1000;
    wtap_hdr.caplen = pkt_hdr->caplen;
    wtap_hdr.len = pkt_hdr->len;
    wtap_hdr.pkt_encap = WTAP_ENCAP_ETHERNET;

    tvbuff_t * tvb = tvb_new_real_data(pkt_data, pkt_hdr->caplen, pkt_hdr->caplen);

    frame_data fdlocal;
    frame_data_init(&fdlocal, pctxt->packet_count, &wtap_hdr, -1, pctxt->byte_count);

    epan_dissect_t * epan_dsct = epan_dissect_new(pctxt->epan_session, 1, 1);
    if (pctxt->dfcode)
    {
        epan_dissect_prime_with_dfilter(epan_dsct, pctxt->dfcode);
    }

    const frame_data * ref = nullptr;
    frame_data_set_before_dissect(&fdlocal, &pctxt->elapsed, &ref, nullptr);

    epan_dissect_run_with_taps(epan_dsct, WTAP_FILE_TYPE_SUBTYPE_PCAP, &wtap_hdr, tvb, &fdlocal, nullptr);

    if (pctxt->dfcode)
    {
        passed = dfilter_apply_edt(pctxt->dfcode, epan_dsct);
    }

    if (passed)
    {
        frame_data_set_after_dissect(&fdlocal, &pctxt->byte_count);

        char * pdml_buffer = nullptr;
        size_t allocated_size = 0;

        FILE * pfBuff = open_memstream(&pdml_buffer, &allocated_size);
        if (nullptr != pfBuff)
        {
            static output_fields_t * NO_OUTPUT_FIELDS = nullptr;
            static const gint DONT_PRINT_HEX = 0;
            static gchar ** NO_PROTOCOL_FILTER = nullptr;
            static column_info * NO_COLUMN_INFO = nullptr;
            
            write_json_proto_tree(NO_OUTPUT_FIELDS,
                                  print_dissections_expanded,
                                  DONT_PRINT_HEX,
                                  NO_PROTOCOL_FILTER,
                                  PF_INCLUDE_CHILDREN,
                                  epan_dsct,
                                  NO_COLUMN_INFO,
                                  proto_node_group_children_by_json_key,
                                  pfBuff);

            fwrite("\n", 1, 1, pfBuff);
            fclose(pfBuff);

            if (nullptr != pdml_buffer && 0 != allocated_size)
            {
                outBuf.assign(pdml_buffer, pdml_buffer + allocated_size);
                free(pdml_buffer);
                pdml_buffer = nullptr;
                allocated_size = 0;
                result = 0;
            }
        }
    }
    epan_dissect_free(epan_dsct);
    frame_data_destroy(&fdlocal);
    wtap_phdr_cleanup(&wtap_hdr);

    return result;
}

size_t pcap2pdml::get_parser_used_mem()
{
    return epan_get_file_used_mem();
}

size_t pcap2pdml::get_epan_used_mem()
{
    return epan_get_used_mem();
}

//-----------------------------------------------------------------------------------------//

int pcap2pdml::term_parser(void * parser_ctxt)
{
    auto * pctxt = (parser_context_t *)parser_ctxt;
    if (nullptr != pctxt)
    {
        if (nullptr != pctxt->dfcode)
        {
            dfilter_free(pctxt->dfcode);
            pctxt->dfcode = nullptr;
        }
        if (nullptr != pctxt->epan_session)
        {
            epan_free(pctxt->epan_session);
            pctxt->epan_session = nullptr;
        }
        free(parser_ctxt);
    }
    epan_cleanup();
    return 0;
}

//=========================================================================================//

