

#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <pthread.h>
#include <pcap.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/uio.h>
#include <string>
#include <pcap/pcap.h>
#include "logger.h"
#include "EpanManager.h"
#include "statistics.h"

static const std::string EMPTY_STRING = "Empty";
static const int SYNC_STR_SIZE = 4;

static const int PacketHeaderSize = sizeof(pcap_pkthdr);

static const std::string strSync = {0x7a, 0x7a, 0x7a, 0x7a};

EpanManager::EpanManager(const std::string & wireshark_profile, 
                         const std::string & dfilter, 
                         const size_t p_max_8mb_blocks, 
                         const std::string & p_ID) :
    ws_profile(wireshark_profile),
    ws_dfilter(dfilter),
    max_8mb_blocks(p_max_8mb_blocks),
    ID(p_ID),
    isMirror(true),
    counter_print(0)
{
    LOG(_DEBUG) << "PacketHeaderSize: " << PacketHeaderSize;
  
    is_running = false;
}

void EpanManager::check_epan_reset()
{
    check_epan_reset(pcap2pdml_ctxt, 
                     max_8mb_blocks, 
                     ws_dfilter.c_str());
}

void EpanManager::check_epan_reset(void * pcap2pdml_ctxt, 
                                   const size_t epan_max_8MB_blocks, 
                                   const char * ws_dfilter)
{
#define _8MB_ 8*1024*1024

    /*
     * Checking the global 'epan-reset' flag OR the exceeding of the max 8MB memory blocks
     */
    bool epan_reset_flag = __sync_bool_compare_and_swap(&epan_reset_flag, 1, 0);
    if (!epan_reset_flag)
    {
        size_t used_mem_file = pcap2pdml::get_parser_used_mem(), used_mem_epan = pcap2pdml::get_epan_used_mem();
        LOG(_DEBUG) << "Epan used memory (file/epan):" << used_mem_file << " / " << used_mem_epan;
        epan_reset_flag = (0 != epan_max_8MB_blocks && (epan_max_8MB_blocks * _8MB_) <= used_mem_file) ? true : false;
    }

    if (epan_reset_flag)
    {
        LOG(_ERROR) << "epan reset flag is set; resetting.";
        if (0 == pcap2pdml::re_init_parser(pcap2pdml_ctxt, ws_dfilter))
        {
            LOG(_INFO) << "pcap2pdml context re-initialized.";
        }
        else
        {
            LOG(_ERROR) << "pcap2pdml context re-initialization failed.";
        }
    }
#undef _8MB_    
}

void EpanManager::init()
{
    if (pcap2pdml::init_parser(&pcap2pdml_ctxt, ws_profile.c_str(), ws_dfilter.c_str()) == -1)
    {
        LOG(_ERROR) << "Cannot init EPAN";
        return;
    }
    is_running = true;
}

void EpanManager::term()
{
    is_running = false;
    if (parser != NULL)
    {
        pcap2pdml::term_parser(pcap2pdml_ctxt);

        pcap2pdml_ctxt = NULL;
    }
}

ssize_t EpanManager::WaitForPacket(int fdr)
{
    // buf will hold the incoming packet.
    memset(buf, 0, sizeof(buf));

    // We wait on the named pipe for raw packets
    timeval tv;
    fd_set rfds;

    while (is_running)
    {
        FD_ZERO(&rfds);
        FD_SET(fdr, &rfds);
        if (fdr == 0)
        {
            LOG(_ERROR) << "Wait for result called before reader was initialized.";
            return 0;
        }

        tv.tv_sec = 10;
        tv.tv_usec = 0;
        ssize_t location = 0;

        ssize_t rv = select(fdr + 1, &rfds, NULL, NULL, &tv);
        if (rv == -1)
        {
            LOG(_ERROR) << "Reading from named pipe failed: " << strerror(errno);
            return 0;
        }
        else if (rv == 0) //timeout occurred
        {
            return 0;
        }
        else
        {
            do
            {
                rv = read(fdr, &buf[location], sizeof(buf) - location);
                if ((rv == -1) && (errno != EAGAIN))
                {
                    LOG(_ERROR) << "recv returned an error: " << ID << ". Error: " << strerror(errno);
                    StatsSingleton::instance().Increment(RecPacketError);
                }
                else if (rv > 0)
                {                  
                    location += rv;
                    if(location > MAX_PACKET_SIZE)
                    {
                        LOG(_ERROR) << "MAX PACKET SIZE REACHED !!! SHOULD NOT OCCUR";
                        break;
                    }
                }
            }
            while (rv > 0);
           
            // We need to test if the packet starts with our SYNC chars: a7a7a7a7
            if (location > 0)
            {
                if (buf[0] != 0xa7 || buf[1] != 0xa7 || buf[2] != 0xa7 || buf[3] != 0xa7)
                {
                    LOG(_ERROR) << "Packet Received with bad sync header";
                    StatsSingleton::instance().Increment(RecPacketBadSync);
                    PrintData(&buf[0], location);
                    return 0;
                }
                StatsSingleton::instance().Increment(RecPacketOk);
            }
            return location;
        }
    }
    return 0;
}

void EpanManager::ProcessPacket()
{
    // ignore the 4 first bytes - header.
    // cast the header structures
    // build the vector
    // send to epan.
    pcap_pkthdr PacketHeader;
    memcpy(&PacketHeader,&buf[SYNC_STR_SIZE],PacketHeaderSize);

    StatsSingleton::instance().Increment(EpanProcessPacket);
    
    if(0 != pcap2pdml::parse_packet(pcap2pdml_ctxt,
                                    &PacketHeader, 
                                    &buf[SYNC_STR_SIZE+PacketHeaderSize], 
                                    Packet_Output))
    {
        LOG(_DEBUG) << "Parse Returned -1";
        StatsSingleton::instance().Increment(EpanError);
        Packet_Output = EMPTY_STRING;
        return;
    }
    
    if (Packet_Output.empty())
    {
        LOG(_DEBUG) << "Parse Returned empty";
        StatsSingleton::instance().Increment(EpanEmpty);
        Packet_Output = EMPTY_STRING;
        return;
    }

    StatsSingleton::instance().Increment(EpanOk);
}

void EpanManager::SendResult(int fdw)
{
    // /proc/sys/fs/pipe-max-size defines the The maximum size (in bytes)
    // of individual pipes that can be set by users without 
    // the CAP_SYS_RESOURCE capability.
    static const uint32_t MAX_PIPE_SIZE = 1048576;
    
    if (Packet_Output.length() > MAX_PIPE_SIZE)
    {
        if( 1 == (++counter_print % 1000) )
        {
            LOG(_ERROR) << "Too big JSON. Discard. Send empty response. "
                        << "Counter print (1:1000): " << counter_print;
        }
        //LOG(_ERROR) << Packet_Output;
        Packet_Output = EMPTY_STRING;
        StatsSingleton::instance().Increment(SentError);
    }

    Packet_Output.append(strSync);
    
    ssize_t written = write(fdw, Packet_Output.c_str(), Packet_Output.length());

    if (written == -1)
    {
        LOG(_ERROR) << "Cannot send JSON back to Sniffer: " << strerror(errno);
        StatsSingleton::instance().Increment(SentError);
    }
    else if (written == 0)
    {
        LOG(_ERROR) << "Nothing written";
        StatsSingleton::instance().Increment(SentError);
    }
    else if ((std::string::size_type)written != Packet_Output.length())
    {
        LOG(_ERROR) << "Written bytes differs from vector size ! " << written << " actual: " << Packet_Output.length();
        StatsSingleton::instance().Increment(SentError);
    }
    else
    {
        StatsSingleton::instance().Increment(SentOk);
    }
}

void EpanManager::PrintData(unsigned char * data, int size)
{
    if (currentLogLevel < log4cpp::Priority::DEBUG)
    {
        return;
    }
    LOG(_DEBUG) << "Received Data: " << size;
    
    char *mybuf = new char[size*3+1];
    memset(mybuf, 0, size*3 + 1);
    for (int i = 0; i < size; ++i)
    {
        sprintf(mybuf+(i*3), "%02x ", data[i]);
    }
    mybuf[size*3] = 0;   
    LOG(_DEBUG) << mybuf;
    delete mybuf;
}

void EpanManager::SetSnifferMode(bool mirror)
{
    isMirror = mirror;
}