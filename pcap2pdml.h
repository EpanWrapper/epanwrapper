#ifndef PCAP2PDML_H
#define	PCAP2PDML_H

#include <string>

class pcap2pdml
{
public:
    /**
     * Should be called first and once in order to initialize
     * the wireshark infrastructure and create a context token for the library client
     * to use in future calls to the parse_packet() method.)
     * @param parser_ctxt
     * @param profilename
     * @param dfilter
     * @return 
     */
    static int init_parser(void * * parser_ctxt, const char * profilename, const char * dfilter);
    
    /**
     * re-initializes an already initialized parser context
     * @param parser_ctxt a parsing context that WAS initialized with init_parser()
     * @param dfilter
     * @return 
     */
    static int re_init_parser(void * parser_ctxt, const char * dfilter);
    
    /**
     * parses a packet to a PDML or JSON formatted text string.
     * @param parser_ctxt a parsing context that WAS initialized with init_parser()
     * @param pkt_hdr (pointer to) a captured packet header (timestamp, capture size, real size)
     * @param pkt_data a captured packet data buffer - must fit the packet header caplen size field.
     * @param outBuf a reference to a string that will receive the PDML or JSON output
     * @return 
     */
    static int parse_packet(void * parser_ctxt, 
                            const struct pcap_pkthdr * pkt_hdr, 
                            const u_int8_t * pkt_data, 
                            std::string & outBuf);
    
    /**
     * retrieves the file-scope used memory size in bytes
     * @return
     */
    static size_t get_parser_used_mem();
    
    /**
     * @return the epan-scope used memory size in bytes.
     */
    static size_t get_epan_used_mem();
    
    /**
     * Should be called to terminate the library infrastructure
     * and release all the allocated resources. It receives as argument the allocated context.
     * Once called, the context is freed and must not be used in subsequent calls.
     * @param parser_ctxt
     * @return 
     */
    static int term_parser(void * parser_ctxt);  
    
private:

};

#endif	/* PCAP2PDML_H */

