#include "statistics.h"
#include "logger.h"
#include "analyzer_config.h"

#include <log4cpp/RollingFileAppender.hh>
#include <log4cpp/PatternLayout.hh>

#include <boost/algorithm/string.hpp>

#include <stdlib.h>
#include <cstdlib>
#include <inttypes.h>

#include "Configuration/ConfigSingleton.h"

log4cpp::Priority::Value StatsLogLevel = log4cpp::Priority::INFO;

// Instantiate the category object you may extract the root category, but it 
// is usually more practical to directly instance a child category
log4cpp::Category& statsCategory = log4cpp::Category::getInstance("stats");
    

static const uint16_t cycleBufferHistorySize = 50;

Statistics::Statistics() : 
    logInterval_(atoi(fides::ConfigSingleton::getConfigurationTree()->get<std::string>(analyzer::Log::StatsIntervalTime).c_str()))
{
    time(&lastLogTime_);
    
    for (int ind = RecPacketOk; ind < LAST_STATSKEY_VALUE; ++ind)
    {
        statsMap_[ind] = AtomicIntPtr(new AtomicInt(0));
    }
}

Statistics::ValueType Statistics::SetValue(StatsKey key, ValueType value)
{
    return statsMap_[key]->exchange(value);
}

Statistics::ValueType Statistics::GetValue(StatsKey key) 
{ 
    return statsMap_[key]->load();
}
        
void Statistics::Increment(StatsKey key, uint16_t delta)
{
    statsMap_[key]->fetch_add(delta);
}

void Statistics::Decrement(StatsKey key, uint16_t delta)
{
    statsMap_[key]->fetch_sub(delta);
}

void Statistics::LogStats()
{
    // The current time
    time_t now;
    time(&now);
    
    if (now - lastLogTime_ < logInterval_)
    {
        return;
    }
    
    lastLogTime_ = now;
    
    std::string log_line;
    
    log_line += rec_log();
    log_line += ", " + epan_log();
    log_line += ", " + sent_log();

    statsCategory << StatsLogLevel << log_line;
}

std::string Statistics::rec_log()
{
    ValueType tmp1 = SetValue(RecPacketOk, 0);
    ValueType tmp2 = SetValue(RecPacketError, 0);
    ValueType tmp3 = SetValue(RecPacketBadSync, 0);
    char buffer[30];
    snprintf(buffer, 30, "rec, %5" PRIu64", %5" PRIu64", %5" PRIu64"", tmp1, tmp2, tmp3);
    return std::string(buffer);
}

std::string Statistics::epan_log()
{
    ValueType tmp1 = SetValue(EpanProcessPacket, 0);
    ValueType tmp2 = SetValue(EpanOk, 0);
    ValueType tmp3 = SetValue(EpanError, 0);
    ValueType tmp4 = SetValue(EpanEmpty, 0);
    ValueType tmp5 = SetValue(EpanRetransmit, 0);
    char buffer[70];
    snprintf(buffer, 70, "epan, %5" PRIu64", %5" PRIu64", %5" PRIu64", %5" PRIu64", %5" PRIu64"", tmp1, tmp2, tmp3, tmp4, tmp5);
    return std::string(buffer);
}

std::string Statistics::sent_log()
{
    ValueType tmp1 = SetValue(SentOk, 0);
    ValueType tmp2 = SetValue(SentError, 0);
    char buffer[30];
    snprintf(buffer, 30, "sent, %5" PRIu64", %5" PRIu64"", tmp1, tmp2);
    return std::string(buffer);
}

void Statistics::Start()
{
    isRunning_.store(true);
    statisticsThread_.reset(new boost::thread(boost::ref(*this)));
}

void Statistics::Stop()
{
    // Signal the thread to stop (thread-safe)
    isRunning_.store(false);
    
    // Wait for the thread to finish.
    if (statisticsThread_ != NULL) 
    {
        statisticsThread_->join();
    }
    
    statsCategory << StatsLogLevel << "-- Stop fides-analyzer --";
}

void Statistics::operator () ()
{
    LOG(_DEBUG) << "Start Statistics";
    uint32_t waitCount = 0;
    while (isRunning_.load())
    {
        usleep(5000);
        waitCount += 1;
        if (waitCount >= 100)
        {
            waitCount = 0;
            LogStats();
        }
    }
    
    LOG(_DEBUG) << "Stop Statistics";
}

void Statistics::CreateStatsLog(const std::string & ID)
{
    std::string appenderName = "stats";
    bool append = true;
    const std::string logfilename = analyzer::StatsFileNameBase + ID + ".log";
    
    // Instantiate an appender object that will append to a log file
    log4cpp::Appender *appender = new log4cpp::RollingFileAppender (
            appenderName, 
            logfilename,
            atoi(fides::ConfigSingleton::getConfigurationTree()->get<std::string>(analyzer::Log::MaxFileSize).c_str()),
            atoi(fides::ConfigSingleton::getConfigurationTree()->get<std::string>(analyzer::Log::FileCount).c_str()),
            append);
	
    // Instantiate a layout object
    log4cpp::PatternLayout*patternLayout = new log4cpp::PatternLayout;
    patternLayout->setConversionPattern("%d %-5p %m%n");
    
    // Attach the layout object to the appender object
    appender->setLayout(patternLayout);

    // An Appender when added to a category becomes an additional output 
    // destination unless additivity is set to false. When it is false, the 
    // appender added to the category replaces all previously existing appenders
    statsCategory.setAdditivity(false);
    
    // This appender becomes the only one
    statsCategory.setAppender(appender);
    
    // Set up the priority for the category. Ex if set to INFO priority
    // attempts to log DEBUG messages will fail (no print))
    statsCategory.setPriority(StatsLogLevel);
    
    statsCategory << StatsLogLevel << "-- Start fides-analyzer " << ID << " --";
}

